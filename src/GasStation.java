import java.text.DecimalFormat;
import java.util.Scanner;

public class GasStation {
    private Scanner sc;

    public GasStation() {
        sc = new Scanner(System.in);
    }

    public void startPumping() {
        System.out.println("Hello from the station");

        while (true) {
            double amountOfGas = pumpGas();
            payForGas(amountOfGas);
            System.out.println("END!");
        }

    }

    private double pumpGas() {

        System.out.println("Enter amount of gas: ");
        double amountOfGas = sc.nextDouble();
        System.out.println("Gas: " + amountOfGas);
        System.out.println("More?");
        sc.nextLine();
        String input = sc.nextLine();

        while (input.toLowerCase().equals("more")) {
            System.out.println("Enter amount of gas: ");
            amountOfGas += sc.nextDouble();
            System.out.println("Gas: " + amountOfGas);
            System.out.println("More?");
            sc.nextLine();
            input = sc.nextLine();
        }

        return amountOfGas;
    }

    private void payForGas(double amountOfGas) {
        DecimalFormat df = new DecimalFormat("0.00");
        double needToPay = amountOfGas * 1.21;
        System.out.println("Pay: " + needToPay);
        double money = 0;
        while (true) {
            money += sc.nextDouble();
            double result = needToPay - money;
            if (result > 0) {
                System.out.println("Please add more:" + df.format(result));
            }
            if (result < 0) {
                System.out.println("Too much, take back" + df.format(-result));
                break;
            }
            if (result == 0) {
                System.out.println("Thanks bye");
                break;
            }
        }
    }
}
